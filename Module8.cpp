﻿#include <iostream>
#include <stack>


class Stack
{
	
private:
	int a;
	int* Arr;
	int last_index = 0;

public:

	Stack(int N)
	{
		Arr = new int[N]; //выделение памяти в конструкторе
	}

	void push(int N)
	{
		
		std::cout << "Stack:" << '\n';

		for (int i = 0; i < N; i++)
		{
									
			std::cin >> a;

			Arr[i] = a;
		}

		last_index++;

		if (last_index < N)
		{
			std::cout << "memory is OK" << '\n';
		}
	}

	
	void show(int N)
	{
		for (int i = 0; i < N; i++)
		{
					
			std::cout << Arr[i] << "\t";

		}
	}

	void pop(int& N)
	{
		N--;
		int* newArr = new int[N];
				
			for (int i = 0; i < N; i++)
			{
				newArr[i] = Arr[i];
			}

		
		Arr = newArr;

		std::cout << '\n';

		last_index--;
	}

	void push_back(int& N)
	{

		int E;
		std::cout << '\n' << "new element:" << '\n';
		std::cin >> E;

		int* newArr = new int[N + 1];

		for (int i = 0; i < N; i++)
		{
			newArr[i] = Arr[i];
		}
		newArr[N] = E;

		N++;
		
		Arr = newArr;

		last_index++;

		if (last_index < N)
		{
			std::cout << "memory is OK" << '\n';
		}
	}

	~Stack()
	{
		delete[] Arr; // удаление памяти в деструкторе
	}

};


int main()
{
	int N;
	std::cout << "Print N" << '\n';
	std::cin >> N;

	Stack s(N);

	s.push(N);

	s.show(N);

	s.pop(N);

	s.show(N);

	s.push_back(N);

	s.show(N);
}